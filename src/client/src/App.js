/* esversion: 6 */
import './App.css';
import React from 'react';
import UserTable from './UserTable';


class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      users: []
    }

    const dataUrl = "http://localhost:8000/user";

    const fetchConfig = {
      mode: "cors",
    };

    var responsePromise = fetch(dataUrl, fetchConfig);

    const valuesPromise = responsePromise.then(response => response.json());
    
    valuesPromise.then(
      values => {
        this.setState({users: values});
      }
    );

  }
  render() {
    return (
      <div className="App">
        <h1>User Management</h1>
        <UserTable users={this.state.users} />
      </div>
    );
  }
}

export default App;
